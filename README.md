gdk-pixbuf usage survey
=======================

This is intended to be a survey the use of gdk-pixbuf by client code.

**Sources** - I am using [Debian Code Search][codesearch] to
look for how free software uses gdk-pixbuf.  This only includes code
which is shipped as Debian packages; it will not include, for example,
code that is on GitHub but not in a Debian package, or of course
proprietary code which is not possible to examine.

* [Roadmap] - General roadmap for gdk-pixbuf.

* [Modules] - survey of third-party gdk-pixbuf modules (codecs)
  
* [Animation] - survey of the internal animation loading APIs

Please [mail me][mail] if you find any other third-party modules, or
code that uses gdk-pixbuf in a way different than described above.

[codesearch]: https://codesearch.debian.net
[Roadmap]: src/roadmap.md
[Modules]: src/modules.md
[Animation]: src/animation.md
[mail]: mailto:federico@gnome.org
