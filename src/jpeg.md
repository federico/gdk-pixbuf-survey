JPEG module
===========

The JPEG module is `io-jpeg.c`.  The GDIP (gdiplus) module is divided
between `io-gdip-utils.c` and `io-gdip-jpeg.c`.

## Loading

The Rust crate is
[jpeg-decoder](https://github.com/kaksmet/jpeg-decoder), used by
Servo's [image crate](https://github.com/image-rs/image).

Both `gdk_pixbuf__jpeg_image_load_increment` and
`gdk_pixbuf__jpeg_image_load` seem to support the same option keys.

* `gdk_pixbuf__jpeg_image_load_increment()` supports the "load at
  size" functionality via `jpeg_decompress_struct.scale_denom /
  output_width / output_height`.  This is not supported in
  `gdk_pixbuf__jpeg_image_load`?

* Reads a `comment` option, see `jpeg_get_comment`.  This was
  requested from `jpeg_save_markers (&cinfo, JPEG_COM, 0xffff)`.  The
  jpeg-decoder crate [parses
  this](https://github.com/kaksmet/jpeg-decoder/blob/7feb6aa/src/parser.rs#L466-L473)
  but [ignores the result](https://github.com/kaksmet/jpeg-decoder/blob/7feb6aa4b4db6b0d780d44cfc8d75887cc28a741/src/decoder.rs#L270).

* If `jpeg_decompress_struct.density_unit` is `1` or `2`, reads in
  `x-dpi` and `y-dpi` options from the `X_density/Y_density` fields.
  Rust jpeg-decoder crate doesn't handle this.

* Produces an `orientation` option out of the EXIF datavia
  `jpeg_parse_exif_app1` (and ignores a bunch of unsupported EXIF
  tags?).  This was requested from `jpeg_save_markers (&cinfo,
  JPEG_APP0+1, 0xffff)`.
  
* Produces a `icc-profile` option via `jpeg_parse_exif_app2_segment`.
  This was requested from `jpeg_save_markers (&cinfo, JPEG_APP0+2,
  0xffff)`.
  
* I think both the EXIF and icc-profile markers can be handled in the
  jpeg-decoder's crate [parse_app
  function](https://github.com/kaksmet/jpeg-decoder/blob/7feb6aa4b4db6b0d780d44cfc8d75887cc28a741/src/parser.rs#L476).

### GDIP loader

* The gdiplus loader creates an `orientation` option, in
  `gdip_bitmap_to_pixbuf()`, but doesn't do `x-dpy/y-dpi/icc-profile`
  as io-jpeg.

## Saving

Servo's [image crate](https://github.com/image-rs/image) uses its own
[JPEG encoder](https://github.com/image-rs/image/tree/master/src/jpeg).

`real_save_jpeg()` does the work.  Notably, the `keys` it supports
don't match the ones from
`gdk_pixbuf__jpeg_is_save_option_supported()`; I don't know how
important this is.

* `quality` key.  Doable with `JPEGEncoder::new_with_quality()`.

* `x-dpy`, `y-dpy` keys.  For libjpeg these cause
  `jpeg_compress_struct.{density_unit, X_density, Y_density}` to be
  set.  Image crate doesn't handle this?
  
* `icc-profile` key.  For libjpeg this causes `jpeg_write_marker()` to
  be called.  I think the equivalent in the image crate is is
  `write_segment()`.

### GDIP saver

* The gdiplus saver in `io-gdip-jpeg` handles the `quality` option,
  but does not handle `x-dpy/y-dpi/icc-profile` as io-jpeg.

## Extra notes

There is a very interesting thread around a paid gig for porting
mozjpeg to Rust -
https://users.rust-lang.org/t/porting-mozjpeg-to-rust-10k-gig/28899/8 -
the comments are very interesting.
