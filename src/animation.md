Animation
=========

The only module that implements `load_animation`, among gdk-pixbuf's
built-in modules and the third-party ones mentioned in
[modules.md](modules.md), is the built-in `io-gif.c` loader.

## gdk_pixbuf_animation_new_from_file()

`gdk_pixbuf_animation_new_from_file()` tries the module's available
APIs in this order:

1. `load_animation`; this is only available in `io-gif.c`; that
directly returns a `GdkPixbufAnimation` or an error.

2. `begin_load/load_increment/stop_load` by hand; this
duplicates some logic from `GdkPixbufLoader`; it is basically the same
but without support for the paraphernalia around the `size_func`
callback.

3. `_gdk_pixbuf_generic_image_load` - this takes a `FILE *` and
returns a `GdkPixbuf` or an error.  Notably, this function tries the
module's `load`, `begin_load`, `load_animation`, more or less in
reverse to what `gdk_pixbuf_animation_new_from_file()` does; of course
if called from there it is assumed that the `load` case will get
used.

## GdkPixbufLoader

When a `GdkPixbufLoader` is using the progressive loading API in a module
(`begin_load/stop_load/load_increment`), it gets to know whether the
module is loading a static image or an animation by the arguments
passed to `prepared_func`:

```c
typedef void (* GdkPixbufModulePreparedFunc) (GdkPixbuf          *pixbuf,
                                              GdkPixbufAnimation *anim,
                                              gpointer            user_data);

struct _GdkPixbufModule {
        /* ... */
        gpointer (* begin_load)     (GdkPixbufModuleSizeFunc size_func,
                                     GdkPixbufModulePreparedFunc prepared_func,
                                     GdkPixbufModuleUpdatedFunc updated_func,
                                     gpointer user_data,
                                     GError **error);
        /* ... */
}
```

The module is expected to call `prepared_func` with a non-NULL
`pixbuf` argument; this is checked in the internal
`gdk_pixbuf_loader_prepare` callback.

The `prepared_func` can get an `anim != NULL` argument; in this case
it means that the module detected an animation.

Both `GdkPixbufLoader` and the `gdk_pixbuf_animation_new_*()`
functions wrap non-animations with `gdk_pixbuf_non_anim_new()` so that
they can have a single code path for either.

## Gdip

On Windows, the gdk-pixbuf modules are reimplemented in terms of the
native Gdip API, which uses native codecs.  The progressive loading
API is implemented in a similar fashion to the [third-party
modules](modules.md); `gdk_pixbuf__gdip_image_load_increment`
accumulates into a `GByteArray` and finally
`gdk_pixbuf__gdip_image_stop_load` copies that buffer to an
`HGLOBAL`-backed buffer and calls the Gdip API on it.

There is probably some duplication between `io-gdip-animation.c` and
`io-gif-animation.c`; I haven't verified this yet.  Both subclass the
abstract `GdkPixbufAnimationClass`; maybe the latter should be a
`GTypeInterface` instead.
