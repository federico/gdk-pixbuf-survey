gdk-pixbuf Modules
==================

"Modules" is gdk-pixbuf's terminology for the runtime-loadable modules
that implement image format codecs.  Within the gdk-pixbuf sources
there are modules for JPEG, PNG, GIF, and others; there are also a few
third-party projects that provide their own modules/codecs.

Originally, gdk-pixbuf intended to ship with support for a few very
common image formats (JPEG, PNG, GIF, etc.), and for third-parties to
provide their own modules in case someone implemented support for
"exotic" file formats on their own.

These are the third-party modules I found by looking for
[`GdkPixbufModule` on Debian Code
Search](https://codesearch.debian.net/search?q=GdkPixbufModule&literal=1) -
the name of that type is used in the entry point to gdk-pixbuf
modules:

* [libheif] - ISO/IEC 23008-12:2017 HEIF file format - HEVC (h.265) codec
* [libopenraw] - Codec for camera RAW formats
* [librsvg] - Parser/renderer for SVG documents.
* [libwmf] - Parses/renders the Windows Metafile format.
* lablgtk3 (ocaml binding), gauche-gtk2 (R7RS Scheme binding), rgtk2
  (R binding) - these have `GdkPixbufModule` commented out as a
  placeholder; they don't actually let one implement gdk-pixbuf
  modules in their respective languages.
  
These are third-party modules that got pointed to me; they can be
found on GitHub by searching for `pixbuf`.  Thanks to Adam Dane and
Jan Tojnar for the pointers.

* [apng] - Animated PNG
* [exif-raw] - Extract thumbnails from camera RAW files as pixbufs
* [psd1] - Adobe Photoshop
* [pvr] - PowerVR texture
* [vtf] - Valve texture format
* [webp] - WebP format
* [xcf] - The GIMP's image format

# Comments on third-party modules in general

All the third-party modules implement the progressive loading API:

 * `begin_load`
 * `load_increment`
 * `stop_load`
 
This may be because `GdkPixbufLoader`, the oft-recommended way of loading
images, will assert if `begin_load` is not available.  However,
modules tend to go to great lengths to avoid having to do progressive
loading; for some of them, it is easier to progressively slurp the
whole data into memory, or a temporary file, and decode it in `stop_load`.

The third-party modules vary in terms of which of these methods they provide:

 * `load` 
 * `load_animation`
 * `save`
 * `save_to_callback`
 * `is_save_option_supported`
 
There is one method that only the built-in XPM module is intended to
implement, for historical reasons:

 * `load_xpm_data`

# Comments on individual modules

## apng

[Animated PNG module][apng]

Subclasses `GdkPixbufAnimationClass` into `GdkPixbufAnimApngClass` and
provides the corresponding `GdkPixbufApngAnimIter`.

Implements `load_animation` incompletely; it just creates an object of
type `GDK_TYPE_PIXBUF_APNG_ANIM` but never populates it with the
provided file.

Implements the progressive loading API with
`begin_load/stop_load/load_increment` by actually processing each
sub-buffer on each call to `load_increment`.  Calls `size_func`, but I
think it doesn't use the returned size at all?

Calls `prepared_func` properly with the animation object; does not
call `updated_func` and just has a commented-out call to it.

## exif-raw

[Module for camera RAW files that uses exiv2][exif-raw]

Extracts an embedded preview from a RAW file, uses the ImageMagick
library to convert/scale it to an intermediate image, and then
uses a `GdkPixbufLoader` to decode that intermediate image; the
resulting `GdkPixbuf` is what it returns.

Implements `load` from a `FILE *`.

Implements the progressive loading API with
`begin_load/stop_load/load_increment`.  Its `begin_load` just
creates an empty GByteArray; `load_increment` accumulates bytes into
that array, and finally `stop_load` decodes the image.  Static images only.

Calls `prepared_func` and `updated_func` once, when `stop_load` is
called.


## libheif

[libheif's gdk-pixbuf module source code][libheif]

Implements only the progressive loading API with
`begin_load/stop_load/load_increment`.  Its `begin_load` just
creates an empty GByteArray; `load_increment` accumulates bytes into
that array, and finally `stop_load` calls into libheif itself to
decode the image.  Uses the `size_func` itself.  Static images only.

Calls `prepared_func` and `updated_func` once, when `stop_load` is
called.

## libopenraw

[libopenraw's gdk-pixbuf module source code][libopenraw]

Implements only the progressive loading API with
`begin_load/stop_load/load_increment`.  Its `begin_load` just
creates an empty GByteArray; `load_increment` accumulates bytes into
that array, and finally `stop_load` calls into libopenraw itself to
decode the image.  Does not use the `size_func`.  Static images only.

Calls `prepared_func` and `updated_func` once, when `stop_load` is called.

## librsvg

[librsvg's gdk-pixbuf module source code][librsvg]

Used as a gdk-pixbuf loader by GTK to avoid a direct dependency on
librsvg, for historical reasons.

Implements only the progressive loading API with
`begin_load/stop_load/load_increment`.  Uses the deprecated
`rsvg_handle_write/rsvg_handle_close` APIs; librsvg would prefer to
get a `GInputStream` instead.  Uses the `size_func`.  Static images only.

Calls `prepared_func` and `updated_func` once, when `stop_load` is
called.

## libwmf

[libwmf's gdk-pixbuf module source code][libwmf]

Implements only the progressive loading API with
`begin_load/stop_load/load_increment`.  Its `begin_load` just
creates an empty GByteArray; `load_increment` accumulates bytes into
that array, and finally `stop_load` calls into libwmf itself to
decode/render the image.  Uses the `size_func` itself.  Static images only.

Calls `prepared_func` and `updated_func` once, when `stop_load` is
called.

## psd

Three loaders for Adobe Photoshop files, all from the same code base:

* [jdudek - PSD module][psd1]
* [and-rom - PSD module][psd2]
* [sukimashita - PSD module][psd3]

Implements only the progressive loading API with
`begin_load/stop_load/load_increment` by actually processing each
sub-buffer on each call to `load_increment`.  Static images only.

Calls the `size_func` only to know if it should stop after reading the
image size, i.e. if it returns (0, 0).  Does not use the sizing
information otherwise.

Calls `prepared_func` from within `load_increment`.

Does not call `updated_func`.

## pvr

[PowerVR texture format module][pvr]

Implements `load`.

Implements `save` and actually provides its own options to select a
sub-format within the texture file.  However, does not implement
`is_save_option_supported`.

Implements the progressive loading API with
`begin_load/stop_load/load_increment`.  Its `begin_load` just
creates an empty GArray; `load_increment` accumulates bytes into
that array, and finally `stop_load` decodes the image.

Its `load_increment` uses the `size_func`, apparently incorrectly, to
detect a (0, 0) size.

Calls `prepared_func` from `stop_load`.  Static images only.

Does not call `updated_func`.

## vtf

[Valve Texture Format (VTF) module source code][vtf]

Implements `load`.

Implements the progressive loading API with
`begin_load/stop_load/load_increment`.  Its `begin_load` just
creates an empty GByteArray; `load_increment` accumulates bytes into
that array, and finally `stop_load` decodes the image.

Does not use the `size_func`.

If the texture is non-animated, calls `prepared_func` from `stop_load`
with just the pixbuf.

If the texture is animated, calls `prepared_func` from `stop_load` with a
`GdkPixbufSimpleAnim` built out of animation frames.

Does not call `updated_func`.

## webp

[WebP module][webp]

Implements `load` by slurping the file into memory and then decoding it.

Implements `save` and `save_to_callback`; actually implements
format-specific options.  However, does not implement `is_save_option_supported`.

Implements the progressive loading API with
`begin_load/stop_load/load_increment`; uses progressive decoding in
`load_increment`.  Static images only.

Calls `size_func` and is able to scale the image on its own; does not
use the magic (0, 0) to stop loading and just return the image size.

Calls `prepared_func` from `load_increment`.

Properly calls `updated_func` with the region for the scanlines
decoded in each call to `load_increment`.


## xcf

[GIMP image format module][xcf]

Implements `load`.

Implements the progressive loading API with
`begin_load/stop_load/load_increment` by copying uncompressed XCF data
to a temporary file (or uncompressing gzip or bzip2-compressed XCF
data into a temporary file) from `load_increment`, and finally
decoding the image in `stop_load`.  The `load_increment`
implementation may have an out-of-bounds read if the first call, with
the file headers, obtains a smaller buffer than the header size.

Calls `prepared_func` and `updated_func` once, when `stop_load` is
called.

Does not use `size_func`.




[apng]: https://github.com/berenm/gdk-pixbuf-loader-apng
[exif-raw]: https://github.com/whatdoineed2do/eog-plugin-exif-rating-raw-viewer/tree/master/src/gdk-pixbuf
[libheif]: https://github.com/strukturag/libheif/tree/master/gdk-pixbuf
[libopenraw]: https://gitlab.freedesktop.org/libopenraw/libopenraw/blob/master/gnome/pixbuf-loader.c
[librsvg]: https://gitlab.gnome.org/GNOME/librsvg/tree/master/gdk-pixbuf-loader
[libwmf]: https://github.com/caolanm/libwmf/blob/master/src/io-wmf.c
[psd1]: https://github.com/jdudek/gdk-pixbuf-psd
[psd2]: https://github.com/and-rom/gdk-pixbuf-psd
[psd3]: https://cgit.sukimashita.com/gdk-pixbuf-psd.git/
[pvr]: https://github.com/media-explorer/gdk-pixbuf-texture-tool
[vtf]: https://github.com/linux-source-tools/gimp-plugin-vtf/blob/master/gdkpixbuf-loader-vtf.c
[webp]: https://github.com/aruiz/webp-pixbuf-loader/
[xcf]: https://github.com/StephaneDelcroix/xcf-pixbuf-loader
