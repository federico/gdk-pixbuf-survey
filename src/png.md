PNG module
==========

The PNG module is `io-png.c`.  The GDIP (gdiplus) module is divided
between `io-gdip-utils.c` and `io-gdip.png.c`.

## Loading

Servo's [image crate](https://github.com/image-rs/image) uses the very
popular [png crate](https://github.com/image-rs/image-png) underneath;
there is also a [lodepng
crate](https://github.com/kornelski/lodepng-rust).  Both seem to
include all (?) of
[PngSuite](http://www.schaik.com/pngsuite2011/pngsuite.html) in their
test suite.

* Creates multiple `tEXt::*` options, where each `*` is a suffix for
  the PNG's chunk's key name (from libpng's `png_text` struct).
  
* Creates an `icc-profile` option, from `png_get_iCCP()`.

* Creates `x-dpi` and `y-dpi` options, from `png_get_pHYs()`.

### png crate

The `x-dpy` and `y-dpy` options can be obtained from `Decoder::read_info() ->
Reader::info() -> &Info`; that `Info` has a `pixel_dims` field with
this information.

All chunks are arbitrarily readable with the low-level
`StreamingDecoder::update()`; there is no high-level API to access
arbitrary chunks.  This may unfortunately require duplicating a lot of
the machinery that the high-level API uses to call a
`StreamingDecoder` underneath, i.e. a large part of
`src/decoder/mod.rs`. 

### Other crates to look at

https://users.rust-lang.org/t/new-rust-png-decoding-library/30952/12 -
decoder, advertises as faster than the png crate.

https://users.rust-lang.org/t/another-png-encoder/16644 - encoder.

https://users.rust-lang.org/t/details-on-different-rust-implementations-of-deflate/30701/2 -
discussion on implementations of deflate

## Saving

* `tEXt::*` key, with a suffix instead of `*`.  As an implementation
  detail, uses `PNG_TEXT_COMPRESSION_NONE` for ISO-8859-1 only, or
  `PNG_ITXT_COMPRESSION_NONE` for UTF-8.
  
* `icc-profile` key.

* `compression` key.

* `x-dpi`, `y-dpi` keys.

* All keys from `real_save_png` match the ones in
  `gdk_pixbuf__png_is_save_option_supported`.

### png crate

This bug is about supporting [writing arbitrary
chunks](https://github.com/image-rs/image-png/issues/116).

That bug references [an unfinished
branch](https://github.com/HeroicKatora/image-png/commit/94231537146584f19c0127b21625b4aa72f847dd)
to add support for encoding the pixel dimensions, i.e. our `x-dpy`,
`y-dpi` keys.

There is `Encoder::set_compression()`, which doesn't exactly map to
the 1-9 integers that the `compression` key assumes, but may be easily
mappable.
