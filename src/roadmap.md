Roadmap for gdk-pixbuf
======================

This is a combination of "historical reasons" document, and a possible roadmap for
evolving gdk-pixbuf.

## GTK considerations

In theory GTK would be happy to load its own images internally, which are mostly trusted
icons.  However, it also needs applications to be able to feed it images, both directly
loaded from files/streams/whatever and generated by the application.

For the purposes of gdk-pixbuf we can mostly ignore `GdkTexture`.  It is an immutable,
long-lived image in whatever format GTK or the GPU prefer.  There is
`gdk_texture_download()`, which produces data in Cairo's ARGB32 format, but that's not
directly relevant to gdk-pixbuf.

GTK already provides `gdk_texture_new_from_file()` and `gdk_texture_new_from_resource()`.
They can do whatever they want; currently they use gdk-pixbuf.

There is `gdk_texture_new_for_pixbuf()`, which the two functions just described use
internally.  There is also `gdk_texture_new_for_surface(cairo_surface_t *)` that takes an
image surface.

As an optimization, we may want to [reduce the number of pixel format
conversions][conversions].  Note that that article talks about making Cairo image surface
the internal representation of a `GdkPixbuf`; this is not really necessary, since it may
be inconvenient to link gdk-pixbuf to cairo, as it brings in xlib and friends.  Still,
keeping the same pixel format internally may be useful to avoid conversions along the
pipeline.

## API considerations

Applications have found it useful to have a "load me an image" API with minimal fuss.
In addition to this, gdk-pixbuf loads ancillary data like an ICC profile, but
unfortunately most applications ignore it.

Applications do use the easy "save me an image" API, in particular thumbnailers.  FIXME:
see where this is used.

## GdkPixbuf internals

The `GdkPixbuf` object is opaque, even to built-in loaders.  We can change it arbitrarily
while maintaining the API.

Since 2010 (!) there was a [proposal to make the internal representation the same as
Cairo's][havoc].  This has not happened yet, but is fully compatible with the wish to
[reduce the number of pixel format conversions][conversions].

Right now `GdkPixbuf` can be in the following states:

```
typedef enum {
        STORAGE_UNINITIALIZED,
        STORAGE_PIXELS,
        STORAGE_BYTES
} Storage;

struct _GdkPixbuf {
        struct {
                Pixels pixels;
                Bytes bytes;
        } s;
};
```

**Note:** `s` is not a union because the when GObject initializes every property, it will
set both pixels and bytes to NULL, or either one of them to non-NULL if the application
sets the appropriate construct-time property, .  Later, `::constructed()` will figure out
which one to use and will finish initializing the object.

**pixels** is when the pixbuf is mutable; it stores a `guchar *` buffer and a `destroy_fn`
as provided by `gdk_pixbuf_new_from_data`.

**bytes** is when the pixbuf is immutable; it stores a `GBytes`.

In theory immutable pixbufs are preferred, but those created from loaders tend to be all
mutable, since the loaders call `gdk_pixbuf_new()` and later fill in the buffer.  We
should have a way to make a pixbuf immutable.

For API compatibility reasons, an immutable pixbuf can `downgrade_to_pixels()` if required
by e.g. calling `gdk_pixbuf_get_pixels()` on it.  We should look for applications that do
this and change them.

## To-do list

* Add a way to turn a mutable pixbuf into an immutable one.

* Make at least the built-in loaders produce immutable pixbufs.  Maybe they can be changed
  to use `gdk_pixbuf_new_from_bytes()` directly, if they can avoid the initial conversion
  from the codec's pixel format to RGBA.  Otherwise they can turn the mutable pixbuf into
  immutable as per above.

* Change the internal representation of `GdkPixbuf` to be Cairo-friendly.  This requires
  `downgrade_to_pixels()` to do an ARGB32 -> RGBA conversion.

* Look for applications which call `gdk_pixbuf_get_pixels()` /
  `gdk_pixbuf_get_pixels_with_length()` and change them to only use immutable pixbufs or
  Cairo surfaces directly.  See what they are actually doing; are they loading an image
  with gdk-pixbuf, modifying it, and sending it back to GTK?  If it's colorizing an icon,
  maybe that can be removed altogether since GTK no longer needs that.  Are they
  generating new images?  Have them make the pixbuf immutable.
  

[conversions]: https://people.gnome.org/~federico/blog/reducing-image-copies.html
[havoc]: https://bugzilla.gnome.org/show_bug.cgi?id=491507
